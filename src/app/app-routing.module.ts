import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommentsectionComponent } from './commentsection/commentsection.component';
import { HeadingComponent } from './heading/heading.component';

const routes: Routes = [
  {path:'', component: HeadingComponent},
  {path:'comments', component: CommentsectionComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
