import { Component, OnInit } from '@angular/core';
import { NewsapiservicesService } from '../service/newsapiservices.service';

@Component({
  selector: 'app-heading',
  templateUrl: './heading.component.html',
  styleUrls: ['./heading.component.scss']
})
export class HeadingComponent implements OnInit {

  constructor(private _services:NewsapiservicesService) { }

  headingNewsArray:any = [];

  ngOnInit(): void {
    
    this._services.heading().subscribe((result)=>{
      console.log(result);
      this.headingNewsArray = result.hits;
    })

  }

}
