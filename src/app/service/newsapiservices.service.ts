import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NewsapiservicesService {

  constructor(private _http:HttpClient) { }

  newsApiUrl = "http://hn.algolia.com/api/v1/search?tags=front_page";

  heading():Observable<any> {
    return this._http.get(this.newsApiUrl);
  }

}
